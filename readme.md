# Emacs Keybindings  
Lists the most important keybindings I use in Emacs so a new Emacs user will have a easy to read reference to the keybindings.

## File Editing  
* **C - x	C - f**:	Open a new file or a existing file.  
* **C - x C - s**:	Save current buffer  
* **C - x b**:		Switch Buffer  
* **C - g**:		Cancel any Emacs command  
  
## Navigation  
* **C - a**:	Go to start of the line  
* **C - e**:	Go to end of the line.  
* **C - k**:	Cut/Delete from cursor current position to the end of the line.  
* **M - <**:	Move to top of buffer  
* **M - >**:	Move to Bottom of buffer  
  
## Search  
* **C - s**:	Forward Search  
* **C - r**:	Backward Search  
* **C - s**:	Jump to next occurrence  
* **C - r**:	Jump to previous occurrence  
* **C - g**:	Exit search  
  
## Select, Copy, Cut and Paste  
* **C - Space**:	Begin Selection  
* **C - G**:		Cancel Selection  
* **C - _**:	Undo  
* **C - x u**:	Redo  
* **M - w**:		Copy  
* **C - y**:		Paste (Yank)  
* **C - w**:		Cut (Wipe out), Delete and copy to clipboard (Kill Ring)  
  
## Text Editing  
* **C - y**:		Copy selected text by cursor (Copy region)  
* **C - y**:		Paste when no text is selected. (Yank)  
  
## Resource  
* [Emacs Key Bindings](https://caiorss.github.io/Emacs-Elisp-Programming/Keybindings.html)
